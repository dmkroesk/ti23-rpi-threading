#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

void *myThreadFunction(void *ptr);

int main()
{
	pthread_t t1, t2;
	int iret1, iret2;

	iret1 = pthread_create(&t1, NULL, myThreadFunction, (void *)"Thread 1");
	iret2 = pthread_create(&t2, NULL, myThreadFunction, (void *)"Thread 2");

	// to do: check returns!!

	return 0;
}

void *myThreadFunction(void *ptr)
{
	char *message = (char *) ptr;
	printf("%s \n", message);

	return NULL;
}