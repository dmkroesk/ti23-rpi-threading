#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>

void langdurendeUserFunctie(int duur)
{
	printf(">> langdurendeUserFunctie(%d)\n", duur);
	sleep(duur);
	printf("<< langdurendeUserFunctie() done\n\n");
}

void *WorkerThread(void *ptr)
{
	langdurendeUserFunctie( (* (int*) ptr) );
	return NULL;
}

int main()
{
	pthread_t t1, t2, t3, t4, t5;
	
	int delayA = 4;
	int delayB = 3;

	// nu met threads
	pthread_create(.... &delayA);
	....

	// Wacht totdat threads zijn afgelopen
	pthread_join(t1,NULL);
	....

	return 0;
}


