// producer - consumer 

#include <pthread.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>

// Gemeenschappelijke variabele
int buffer = 0;

// Functie proto's, dit zijn de thread workers ..
void *producer (void *args);
void *consumer (void *args);

pthread_mutex_t pcMutex;
// pthread_cond_t condProd, condCons;

// Producer produceert x aantal datapoints
void *producer(void *args)
{
	// begin met produceren ..
	while(1==1)
	{
		pthread_mutex_lock(&pcMutex);
		
		// produceer
		buffer++;
		printf(">>produceer -> %d\n", buffer);
		
		// Signal consumer dat er iets nieuws is geproduceerd
		// ...
		
		// En wacht totdat consumer deze heeft geconsumeerd
		// ...

		pthread_mutex_unlock(&pcMutex);

		usleep(500000);

	}
}

void *consumer(void *arg)
{
	// begin met consumeren ..
	while(1==1)
	{
		pthread_mutex_lock(&pcMutex);
		
		// Signal producer dat de consumer klaar is om te consumeren
		pthread_cond_signal(&condProd);

		// Wacht zelf op een nieuw bericht (om te consumeren)
		pthread_cond_wait(&condCons, &pcMutex);

		// ... en bij wakker wordern: consumeer
		printf("\t\t\t>>consumeer <- %d\n", buffer);
		
		pthread_mutex_unlock(&pcMutex);
	}
}

int main()
{
	pthread_t t1 = NULL;
	pthread_t t2 = NULL;

	//
	pthread_mutex_init(&pcMutex, NULL);

	// Init conditions ..
	// ex. pthread_cond_init(&condProd, NULL);

	pthread_create(&t1, NULL, producer, NULL);
	pthread_create(&t2, NULL, consumer, NULL);

	pthread_join(t1, NULL);
	pthread_join(t2, NULL);

	return 0;
}