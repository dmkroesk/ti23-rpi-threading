#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>

void *schrijf_bij(void *ptr);
void *schrijf_af(void *ptr);


int rekening = 100;

// Use mutex

int main()
{
	pthread_t t1, t2, t3, t4, t5;
	int iret1, iret2;

	// Bad code?
	iret1 = pthread_create(&t1, NULL, &schrijf_af, NULL);
	iret2 = pthread_create(&t2, NULL, &schrijf_bij, NULL);
	pthread_create(&t3, NULL, &schrijf_bij, NULL);
	pthread_create(&t4, NULL, &schrijf_bij, NULL);
	pthread_create(&t5, NULL, &schrijf_bij, NULL);

	// Wacht totdat threads zijn afgelopen
	pthread_join(t1,NULL);
	pthread_join(t2,NULL);

	return 0;
}

void *schrijf_af(void *ptr)
{
	// to do: sync
	printf("Thread A:\n");
	printf("temp = rekening;\n"); 
	printf("temp = temp - 50;\n");
	printf("rekening = temp;\n\n");

	return NULL;
}

void *schrijf_bij(void *ptr)
{
	// to do: sync
	printf("\t\t\tThread B:\n");
	printf("\t\t\ttemp = rekening;\n"); 
	printf("\t\t\ttemp = temp + 100;\n");
	printf("\t\t\trekening = temp;\n\n");

	return NULL;
}
